import { URLSearchParams } from "url";
import { Response } from "node-fetch";
import fetch from "node-fetch";

export class TiltifyApi {

    private static readonly apiBase = 'https://tiltify.com/api/v3';

    /**
     * 
     * @param token Tiltify API token
     */
    constructor(private token: string) {}

    protected doGetRequest(path: string, pagination?: TiltifyPagination): Promise<Response> {
        let params = '';
        if(pagination) {
            let urlSearchParams = new URLSearchParams();
            urlSearchParams.append('count', pagination.count.toString());
            if(pagination.before !== undefined) {
                urlSearchParams.append('before', pagination.before.toString());
            }
            if(pagination.after !== undefined) {
                urlSearchParams.append('after', pagination.after.toString());
            }
            params = "?" + urlSearchParams.toString();
        }
        const fullPath = TiltifyApi.apiBase + path + params;
        return fetch(fullPath, { 
            method: 'GET',
            headers: {
                'Authorization': 'Bearer ' + this.token
            }
        })
        .then((response) => {
            if(response.status >= 200 && response.status < 300) {
                return response;
            }
            else if(response.status === 403) {
                throw new Error("Error: Got HTTP 403 when querying Tiltify API; check that your API key is correct.");
            }
            else if(response.status === 404) {
                throw new Error("Error: Got HTTP 404 when querying Tiltity API; check that your campaign ID is correct.");
            }

            throw new Error(`Error: Got HTTP ${response.status} ${response.statusText} when querying Tiltify API.`);
        });
    }

    public getDonations(campaignId: number, pagination: TiltifyPagination) : Promise<TiltifyDonation[]> {
        return this.doGetRequest(`/campaigns/${campaignId}/donations`, pagination)
            .then((response) => response.json())
            .then((parsed) => {
                if(!parsed.data) {
                    throw new Error(`Error retrieving Tiltify donations (check your configuration!)\nTiltify API response: ${JSON.stringify(parsed)}`);
                }
                return parsed.data as TiltifyDonation[];
            });
    }

    /**
     * 
     * @param campaignId The campaign ID to watch for donations.
     * @param interval Polling interval.
     * @param callback The function to call when a donation happens.
     * @param emitOnStartup If true, calls the callback for the most recent donation on the first polling cycle.
     *     This is useful for testing without having to repeatedly make donations.
     */
    public getDonationStream(campaignId: number, 
                             interval: number, 
                             callback: (donation: TiltifyDonation) => any, 
                             emitOnStartup: boolean = false): NodeJS.Timeout {

        let lastDonationId: number | undefined = undefined;
        let discard: boolean = !emitOnStartup;
        let requestInProgress: boolean = false;
        return setInterval(async () => {
            if(requestInProgress) {
                console.log("WARNING: Cancelling a Tiltify API request because the previous one is still in progress. If you see this message repeatedly, your poll interval is too small.");
                return;
            }
            let donations;
            try {
                requestInProgress = true;
                donations = await this.getDonations(campaignId, {count: 1, after: lastDonationId});
            }
            catch (error) {
                console.log(error instanceof Error ? error.message : error);
                return;
            }
            finally {
                requestInProgress = false;
            }
            if(donations.length > 0) {
                lastDonationId = donations[0].id;
                // If it's the very first iteration, the donation is probably old.
                // Don't invoke the callback unless the user has enabled this for testing.
                if(!discard) {
                    callback(donations[0]);
                }
            }
            discard = false;
        }, interval);
    }
}

export interface TiltifyDonation {
    id: number;
    amount: number;
    name: string;
    comment: string;
    completedAt: number;
    rewardId?: number;
}

export interface TiltifyPagination {
    count: number;
    before?: number;
    after?: number;
}
