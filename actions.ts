import { Rcon } from "rcon-client/lib";
import { tiltifyRconConfiguration } from "./config";
import { ActionConfiguration } from "./config-types";

// sort backwards so highest minimum donations come first
const sortedActions = tiltifyRconConfiguration.actions.sort((a, b) => 
    // first sort by donation amount
    (b.minDonation - a.minDonation) ||
    // if donation amount is equal, check whether b requires exact amount;
    // if it does, sort it before a.
    (b.exactAmount ? 1 : -1)
);

export function processDonation(donation: { amount: number; }, rconClient: Rcon): void {
    console.log(donation);
    const action = sortedActions.find((actionConfig) =>
        actionConfig.minDonation === donation.amount ||
        actionConfig.minDonation <= donation.amount && !actionConfig.exactAmount
    );
    if(action) {
        action.rconCommands.forEach((cmd) => {
            console.log(`Sending rcon command: ${cmd}`);
            rconClient.send(cmd).catch((reason) => {
                console.log(`Failed to send RCON command. Reason: ${reason instanceof Error ? reason.message : reason}`);
            });
        });
    }
}